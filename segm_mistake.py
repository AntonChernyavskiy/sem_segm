from skimage.io import imread, imsave
from skimage.segmentation import felzenszwalb, slic, quickshift, mark_boundaries
from skimage.util import img_as_float, img_as_ubyte
import matplotlib.pyplot as plt
import pickle


def remember(segments):
    with open('data.txt', 'wb') as f:
        pickle.dump(segments, f)


def read():
    with open('data.txt', 'rb') as f:
        segments = pickle.load(f)
    return segments


def segmentation(image):
    classes = dict()
    classes[(255, 255, 255)] = 0
    classes[(0, 0, 255)] = 1
    classes[(0, 255, 255)] = 2
    classes[(0, 255, 0)] = 3
    classes[(255, 255, 0)] = 4
    classes[(255, 0, 0)] = 5

    ground_truth = imread("data.jpg")[3000:3600, 3000:3600]
    num_segments = 5000
    segments_slic = slic(image, n_segments=num_segments, compactness=10)

    #remember(segments_slic)
    #imsave("sup.png", mark_boundaries(image, segments_slic, color=(0, 0, 0)))
    #segments_slic = read()

    result = [[0, 0, 0, 0, 0, 0] for _ in range(int(num_segments * 1.1))]
    #print(len(segments_slic), len(segments_slic[0]), segments_slic[-1][-1])
    for i in range(len(segments_slic)):
        for j in range(len(segments_slic[i])):
            gr = (int(ground_truth[i][j][0]), int(ground_truth[i][j][1]), int(ground_truth[i][j][2]))
            result[segments_slic[i][j]][classes[gr]] += 1
    return result


def find_mistake(result):
    mistake = 0
    for el in result:
        maximum = 0
        for i in range(len(el)):
            if el[i] > el[maximum]:
                maximum = i
            mistake += el[i]
        mistake -= el[maximum]
    print(mistake)


image = imread("pict.png")
im = image[3000:3600, 3000:3600]
find_mistake(segmentation(im))


#segments_quick = quickshift(image, ratio=1.0, kernel_size=5, max_dist=15, sigma=0)
#segments_felz = felzenszwalb(image, scale=90.0, sigma=0.7, min_size=20)
#plt.imshow(mark_boundaries(image, segments, color=(1, 1, 1)))
#plt.show()

#imsave("felzenszwalb.jpg", mark_boundaries(image, segments_felz, color=(1, 1, 1)))
#imsave("slic.jpg", mark_boundaries(image, segments_slic, color=(1, 1, 1)))
#imsave("quickshift.jpg", mark_boundaries(image, segments_quick, color=(1, 1, 1)))
