from skimage.io import imread, imsave
from skimage.segmentation import felzenszwalb, slic, quickshift, mark_boundaries
from skimage.util import img_as_float, img_as_ubyte
import pickle


def read():
    with open('data.txt', 'rb') as f:
        segments = pickle.load(f)
    return segments


def make_graph(segments, num_segments):
    graph = [set() for _ in range(int(num_segments * 1.1))]
    for i in range(len(segments)):
        for j in range(i % 2, len(segments[i]), 2):
            variants = [[-1, 0], [1, 0], [0, -1], [0, 1], [-1, -1], [1, 1], [-1, 1], [1, -1]]
            for el in variants:
                if 0 < (i + el[0]) < len(segments) and 0 < (j + el[1]) < len(segments[i]):
                    neighbour = segments[i + el[0]][j + el[1]]
                    if neighbour != segments[i][j]:
                        graph[segments[i][j]].add(neighbour)
    print(graph)


image = imread("pict.png")
im = image[3000:3600, 3000:3600]
#num_segments = 5000

#segments_slic = slic(image, n_segments=num_segments, compactness=10)
segments_slic = read()

make_graph(segments_slic, num_segments)