from skimage.io import imread, imsave
from skimage.segmentation import slic, mark_boundaries
from skimage.util import img_as_float, img_as_ubyte
from skimage.filters import gabor, gaussian
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from pystruct.inference import inference_qpbo
import pickle
import os
import numpy as np
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)


def remember(result, name):
    with open(name + '.txt', 'wb') as f:
        pickle.dump(result, f)


def read(name):
    with open(name, 'rb') as f:
        result = pickle.load(f)
    return result


def dispersion(image, nDSM, segments, num_segments, medium_colors, medium_brightness, medium_height, pixels_in_segments):
    disp = [[0, 0, 0, 0, 0] for _ in range(num_segments)]
    for i in range(len(segments)):
        for j in range(len(segments[i])):
            for k in range(3):
                disp[segments[i][j]][k] += (int(image[i][j][k]) - medium_colors[segments[i][j]][k]) ** 2
            brightness = 0.3 * image[i][j][0] + 0.58 * image[i][j][1] + 0.12 * image[i][j][2]
            disp[segments[i][j]][3] += (brightness - medium_brightness[segments[i][j]]) ** 2
            disp[segments[i][j]][4] += (int(nDSM[i][j]) - medium_height[segments[i][j]]) ** 2
    for i in range(num_segments):
        if pixels_in_segments[i] != 0:  
            for k in range(5):
                disp[i][k] /= pixels_in_segments[i]
    return disp


def medium_parametrs(image, num, nDSM, segments, num_segments, save):
    if os.path.exists("Vaihingen/features/vaihingen_" + num + ".txt") and save:
        results = read("Vaihingen/features/vaihingen_" + num + ".txt")       
    else:
        medium_colors = [[0, 0, 0] for _ in range(num_segments)]
        pixels_in_segments = [0 for _ in range(num_segments)]
        medium_height = [0 for _ in range(num_segments)]
        medium_NDVI = [0 for _ in range(num_segments)]
        for i in range(len(segments)):
            for j in range(len(segments[i])):
                pixels_in_segments[segments[i][j]] += 1
                medium_colors[segments[i][j]][0] += int(image[i][j][0])
                medium_colors[segments[i][j]][1] += int(image[i][j][1])
                medium_colors[segments[i][j]][2] += int(image[i][j][2])
                medium_height[segments[i][j]] += int(nDSM[i][j])
        medium_brightness = [0 for _ in range(num_segments)]
        for i in range(num_segments):
            if pixels_in_segments[i] != 0:
                medium_colors[i][0] = int(medium_colors[i][0] / pixels_in_segments[i])
                medium_colors[i][1] = int(medium_colors[i][1] / pixels_in_segments[i])
                medium_colors[i][2] = int(medium_colors[i][2] / pixels_in_segments[i])
                medium_height[i] /= pixels_in_segments[i]
                medium_NDVI[i] /= pixels_in_segments[i]
            medium_brightness[i] = 0.3 * medium_colors[i][0] + 0.58 * medium_colors[i][1] + 0.12 * medium_colors[i][2]
        disp = dispersion(image, nDSM, segments, num_segments, medium_colors, medium_brightness, medium_height, pixels_in_segments)
        
        results = [medium_colors, medium_brightness, medium_height, disp]
        if save:
            remember(results, "Vaihingen/features/vaihingen_" + num)         
    return [results[0], results[1], results[2], results[3]]


def correct_classes(segments, num, correct):
    classes = dict()
    classes[(255, 255, 255)] = 0
    classes[(0, 0, 255)] = 1
    classes[(0, 255, 255)] = 2
    classes[(0, 255, 0)] = 3
    classes[(255, 255, 0)] = 4
    classes[(255, 0, 0)] = 5
    cl = [(255, 255, 255), (0, 0, 255), (0, 255, 255), (0, 255, 0), (255, 255, 0), (255, 0, 0)]
    result = [[0, 0, 0, 0, 0, 0] for _ in range(num)]
    for i in range(len(segments)):
        for j in range(len(segments[i])):
            gr = (int(correct[i][j][0]), int(correct[i][j][1]), int(correct[i][j][2]))
            result[segments[i][j]][classes[gr]] += 1
    for i in range(num):
        maximum = 0
        for j in range(6):
            if result[i][j] > result[i][maximum]:
                maximum = j
        result[i] = maximum
    return result


def full_result(res, num):
    result = [[] for _ in range(num)]
    for i in range(num):
        result[i] += res[0][i]
        for j in range(1, 3):
            result[i] += [res[j][i]]
        result[i] += res[3][i]
    return result


def get_data(num):
    nDSM = imread("Vaihingen/Vaihingen_NDSM/ndsm_09cm_matching_area" + num + ".bmp")
    im = imread("Vaihingen/top/top_mosaic_09cm_area" + num + ".tif")
    correct = imread("Vaihingen/gts_for_participants/top_mosaic_09cm_area" + num + ".tif")
    return (nDSM, correct, im)


def get_slic(num, image, save):
    if os.path.exists("Vaihingen/slic/vaihingen_" + num + ".txt") and save:
        segments = read("Vaihingen/slic/vaihingen_" + num + ".txt")
    else:
        segments = slic(image, n_segments=20000, compactness=10)
        if save:
            remember(segments, "Vaihingen/slic/vaihingen_" + num)
    num_of_segments = np.max(segments) + 1
    return (segments, num_of_segments)


def find_edges(segments, num_of_segments, num, save):
    if os.path.exists("Vaihingen/edges/top_vaihingen_" + num + ".txt") and save:
        edges = read("Vaihingen/edges/top_vaihingen_" + num + ".txt")
    else:
        graph = [set() for _ in range(num_of_segments)]
        for i in range(len(segments)):
            for j in range(i % 2, len(segments[i]), 2):
                variants = [[-1, 0], [1, 0], [0, -1], [0, 1], [-1, -1], [1, 1], [-1, 1], [1, -1]]
                for el in variants:
                    if 0 < (i + el[0]) < len(segments) and 0 < (j + el[1]) < len(segments[i]):
                        neighbour = segments[i + el[0]][j + el[1]]
                        if neighbour != segments[i][j]:
                            graph[segments[i][j]].add(neighbour)
                            graph[neighbour].add(segments[i][j])
        edges = []
        for vertex in range(num_of_segments):
            for neighbour in graph[vertex]:
                edges += [np.array([vertex, neighbour])]
        if save:
            remember(edges, "Vaihingen/edges/top_vaihingen_" + num)        
    return edges


def ConditionalRandomField(unary_potentials, pairwise_potentials, segments, num_of_segments, num, cor_classes, im, save):
    unary_potentials = np.array(unary_potentials)
    pairwise_potentials = np.array(pairwise_potentials)    
    edges = np.array(find_edges(segments, num_of_segments, num, save))
    res = inference_qpbo(unary_potentials, pairwise_potentials, edges)
    color = [(255, 255, 255), (0, 0, 255), (0, 255, 255), (0, 255, 0), (255, 255, 0), (255, 0, 0)]
    classes = [(0, 0, 0) for _ in range(num_of_segments)]
    right = 0
    for i in range(num_of_segments):
        ans = res[i]
        classes[i] = color[ans]
        if ans == cor_classes[i]:
            right += 1
    print(num + " CRF: " + str(right / num_of_segments) + "\n") 
    for i in range(im.shape[0]):
        for j in range(im.shape[1]):
            for k in range(3):
                im[i][j][k] = classes[segments[i][j]][k]
    imsave("Vaihingen/answers/ans_vaihingen_crf_" + num + ".png", im)        


def classifier():
    training_maps = [s for s in input("Enter numbers of training photos: ").split()] #["1", "3", "7", "11", "15", "21", "23", "28", "30"]
    teaching_maps = [s for s in input("Enter numbers of teaching photos: ").split()] #["3", "11", "21", "23", "28", "30", "34"]
    x = input("Do you want to remember results and use the ones that were received earlier? (Y/N) ")
    save = False
    if x == "Y":
        save = True
    features = []
    cor_classes = []
    pairwise_potentials = [[0 for _ in range(6)] for _ in range(6)]
    connections = [0 for _ in range(6)]
    for num in teaching_maps:
        (nDSM, correct, im) = get_data(num)
        (segments, num_of_segments) = get_slic(num, im, save)
        res = medium_parametrs(im, num, nDSM, segments, num_of_segments, save)
        features += full_result(res, num_of_segments)
        correct = correct_classes(segments, num_of_segments, correct)
        cor_classes += correct
        edges = find_edges(segments, num_of_segments, num, save)
        for i in range(len(edges)):
            cl1 = correct[edges[i][0]]
            cl2 = correct[edges[i][1]]
            if cl1 != cl2:
                connections[cl1] += 1
                pairwise_potentials[cl1][cl2] += 1
        print(str(num) + " completed")
    
    for i in range(6):
        for j in range(6):
            if i == j:
                pairwise_potentials[i][j] = 1
            else:
                pairwise_potentials[i][j] = pairwise_potentials[i][j] / connections[i]
        pairwise_potentials[i] = np.array(pairwise_potentials[i])
    #print(pairwise_potentials)

    X = np.array(features)
    Y = np.array(cor_classes)
    print("teaching")
    #model = RandomForestClassifier(n_estimators=100)
    model = GradientBoostingClassifier(n_estimators=250)
    model.fit(X, Y)
    print("teached\n")
    
    for num in training_maps:
        (nDSM, correct, im) = get_data(num)
        (segments, num_of_segments) = get_slic(num, im, save)
        unaries = [[] for _ in range(num_of_segments)]
        res = medium_parametrs(im, num, nDSM, segments, num_of_segments,save)
        features = full_result(res, num_of_segments)
        cor_classes = correct_classes(segments, num_of_segments, correct)
        classes = [(0, 0, 0) for _ in range(num_of_segments)]
        color = [(255, 255, 255), (0, 0, 255), (0, 255, 255), (0, 255, 0), (255, 255, 0), (255, 0, 0)] 
        right = 0
        for i in range(num_of_segments):
            unaries[i] = 6 * model.predict_proba(np.array(features[i]))
            ans = np.argmax(unaries[i])
            classes[i] = color[ans]
            if ans == cor_classes[i]:
                right += 1
        print(num + " GBT: " + str(right / num_of_segments))  
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                for k in range(3):
                    im[i][j][k] = classes[segments[i][j]][k]
        imsave("Vaihingen/answers/ans_vaihingen_" + num + ".png", im)
        ConditionalRandomField(unaries, pairwise_potentials, segments, num_of_segments, num, cor_classes, im, save)


classifier()
