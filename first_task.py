import cv2 as cv

image = cv.imread("test.jpg",1)
cv.imshow("start", image)

gauss = cv.GaussianBlur(image,(3, 3),0)
cv.imshow("gaussian", gauss)

median = cv.medianBlur(image,3)
cv.imshow("median", median)

rows,cols = image.shape[0], image.shape[1]
M = cv.getRotationMatrix2D((cols/2,rows/2),15,0.7)
rotation = cv.warpAffine(image,M,(cols,rows))
cv.imshow("rotation", rotation)

cv.imwrite("gauss.jpg", gauss)
cv.waitKey()