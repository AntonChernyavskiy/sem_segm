from skimage.io import imread, imsave
from skimage.segmentation import felzenszwalb, slic, quickshift, mark_boundaries
from skimage.util import img_as_float, img_as_ubyte
import matplotlib.pyplot as plt


def define_object(color):
    color = (int(color[0]), int(color[1]), int(color[2]))
    if color == (255, 255, 255):
        object = 'impervious surfaces'
    elif color == (0, 0, 255):
        object = 'building'
    elif color == (0, 255, 255):
        object = 'low vegetation'
    elif color == (0, 255, 0):
        object = 'tree'
    elif color == (255, 255, 0):
        object = 'car'
    else:
        object = 'clutter/background'
    return object


def grade(path_res, path_cor):
    result = img_as_ubyte(imread(path_res))
    correct = img_as_ubyte(imread(path_cor))
    score = {}
    variants = ['impervious surfaces', 'building', 'low vegetation', 'tree', 'car', 'clutter/background']
    for el in variants:
        score[el] = [0 for _ in range(4)]

    width = result.shape[1]
    height = result.shape[0]
    for j in range(width):
        for i in range(height):
            res = define_object(result[i, j])
            cor = define_object(correct[i, j])
            if res == cor:
                score[res][0] += 1
                for el in variants:
                    if el != res:
                        score[el][3] += 1
            else:
                score[res][1] += 1
                score[cor][2] += 1
                for el in variants:
                    if el != res and el != cor:
                        score[el][3] += 1

    F1_score = {}
    TP, FP, FN, TN = 0, 0, 0, 0
    for el in variants:
        F1_score[el] = 2 * score[el][0] / (2 * score[el][0] + score[el][1] + score[el][2])
        TP += score[el][0]
        FP += score[el][1]
        FN += score[el][2]
        TN += score[el][3]
    accuracy = (TP + TN) / (TP + TN + FN + FP)

    for el in variants:
        print(el + '   ' + str(F1_score[el]))
    print(accuracy)

grade("res.jpg", "cor.jpg")

'''
image = img_as_float(imread("3.jpg"))

segments_felz = felzenszwalb(image, scale=90.0, sigma=0.7, min_size=20)

segments_slic = slic(image, n_segments=500, compactness=10)

segments_quick = quickshift(image, ratio=1.0, kernel_size=5, max_dist=15, sigma=0)

#plt.imshow(mark_boundaries(image, segments, color=(1, 1, 1)))
#plt.show()
imsave("felzenszwalb.jpg", mark_boundaries(image, segments_felz, color=(1, 1, 1)))
imsave("slic.jpg", mark_boundaries(image, segments_slic, color=(1, 1, 1)))
imsave("quickshift.jpg", mark_boundaries(image, segments_quick, color=(1, 1, 1)))
'''